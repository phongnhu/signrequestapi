﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SignRequest.Api.Domain;
using SignRequest.Api.Configurations;
using SignRequest.Api.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace SignRequest.Api
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<SignRequestDbContext>(options =>
				options.UseInMemoryDatabase(databaseName: "SignRequestDB"));

			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
			services.AddScoped<ISignRequestService, SignRequestService>();
			services.AddScoped<IFileService, MinioDocumentService>();
			services.AddScoped<IEventsService, EventsService>();
			services.AddScoped<IDocumentsService, DocumentsService>();

			services.Configure<MinioOptions>(Configuration.GetSection("Minio"));

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc(
					"v1",
					new Info
					{
						Title = "SignRequest API",
						Version = "v1",
						Contact = new Swashbuckle.AspNetCore.Swagger.Contact
						{
							Name = "INFOdation",
							Email = "contact@infodation.nl",
							Url = "https://www.infodation.nl"
						},
						License = new License
						{
							Name = "Proprietary",
							Url = "https://www.infodation.nl"
						},
						TermsOfService = "https://www.infodation.nl",
						Description = "This is SignRequest API made by INFOdation to process Electronic Signature business"
					});			
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseMvc();

			//This line enables the app to use Swagger, with the configuration in the ConfigureServices method.
			app.UseSwagger(o =>
			{
				o.RouteTemplate = "signrequest/swagger/{documentName}/swagger.json";
			});

			//This line enables Swagger UI, which provides us with a nice, simple UI with which we can view our API calls.
			app.UseSwaggerUI(c =>
			{
				c.RoutePrefix = "signrequest";
				c.SwaggerEndpoint("/signrequest/swagger/v1/swagger.json", "Kikker SignRequest Api v1");
				c.DocumentTitle = "SignRequest APIs";
				
			});
		}
	}
}

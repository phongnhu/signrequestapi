﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Minio;
using SignRequest.Api.Configurations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SignRequest.Api.Services
{
	public class MinioDocumentService: IFileService
	{
		private readonly ILogger logger;
		//private readonly DatabaseContext dbContext;
		private readonly MinioClient minioClient;
		private readonly MinioOptions minioOptions;

		public MinioDocumentService(ILogger<MinioDocumentService> logger, 
			IOptions<MinioOptions> minioOptions)
		{
			this.logger = logger;			
			this.minioOptions = minioOptions.Value;
			//Console.WriteLine($"host {this.minioOptions.Host}, key {this.minioOptions.Key}, secret {this.minioOptions.Secret}");
			minioClient = new MinioClient(this.minioOptions.Host, 
				this.minioOptions.Key, this.minioOptions.Secret);
		}

		public Task GetObject(string bucketName, string fileName, Action<Stream> stream)
		{
			return this.minioClient.GetObjectAsync(bucketName, fileName, stream);
		}

		public Task UploadDocument(string bucketName, string fileName, MemoryStream stream, long size, string contentType = null, Dictionary<string, string> metaData = null)
		{
			Console.WriteLine($"UploadDocument bucketName:{bucketName}, filename:{fileName}");
			return this.minioClient.PutObjectAsync(bucketName, fileName, stream, size, contentType, metaData);
		}
	}
}

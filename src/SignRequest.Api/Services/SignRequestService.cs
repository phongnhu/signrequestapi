﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SignRequest.Api.Domain;
using SignRequest.Model;

namespace SignRequest.Api.Services
{
	public class SignRequestService : ISignRequestService
	{
		private readonly ILogger logger;
		private readonly SignRequestDbContext _context;
		private readonly IFileService fileService;
		private readonly IDocumentsService documentsService;
		private readonly IEventsService eventsService;

		public SignRequestService(ILogger<SignRequestService> logger 
			,SignRequestDbContext context
			,IEventsService eventsService
			,IDocumentsService documentsService
			,IFileService fileService)
		{
			this.logger = logger;
			_context = context;
			this.fileService = fileService;
			this.documentsService = documentsService;
			this.eventsService = eventsService;
		}

		public async Task ProcessEvent(string resellerId, string contractNumber, Event srEvent)
		{
			if (srEvent.EventType == Event.EventTypeEnum.Converted)
			{
				Console.WriteLine($"download converted contract {srEvent.Document.Uuid}...");
				await this.DownloadConvertedContract(resellerId, contractNumber, srEvent.Document.Uuid);
			}

			if (srEvent.EventType == Event.EventTypeEnum.Signed)
			{
				Console.WriteLine($"download signed contract {srEvent.Document.Uuid}...");
				await this.DownloadSignedContract(resellerId, contractNumber, srEvent.Document.Uuid);
				Console.WriteLine($"download signinglog {srEvent.Document.Uuid}...");
				await this.DownloadSigningLog(resellerId, contractNumber, srEvent.Document.Uuid);
			}
		}


		public async Task ProcessEvent(string contractNumber, Event srEvent)
		{
			if (srEvent.EventType == Event.EventTypeEnum.Converted)
			{
				CreateDocumentFromConvertedEvent(contractNumber, srEvent.Document.Uuid);
			}

			var document = documentsService.GetDocument(srEvent.Document.Uuid);
			if (document == null)
			{
				return;
			}
			foreach (var de in document.DocumentEvents)
			{
				if (de.DocumentEventId == srEvent.Uuid)
				{
					return;
				}
			}
			DocumentEvent documentEvent = new DocumentEvent()
			{
				DocumentEventId = srEvent.Uuid,
				Document = document,
				DocumentId = document.Uuid,
				EventDateTime = srEvent.Timestamp,
				Status = srEvent.EventType.ToString(),
				EventRawData = srEvent.ToJson()
			};
			eventsService.SaveNewDocumentEvent(documentEvent);
			UpdateMainDocumentFromEvent(srEvent, document);

			//if (srEvent.EventType == Event.EventTypeEnum.Converted)
			//{				
			//	await this.DownloadConvertedContract(orderId, srEvent.Document.Uuid);
			//}

			//if (srEvent.EventType == Event.EventTypeEnum.Signed)
			//{
			//	await this.DownloadSignedContract(orderId, srEvent.Document.Uuid);
			//	await this.DownloadSigningLog(orderId, srEvent.Document.Uuid);
			//}
		}

		public async Task ProcessEvent(SignRequest.Model.Event srEvent)
		{
			//var document = documentsService.GetDocument(srEvent.Document.Uuid);			

			if (srEvent.EventType == Event.EventTypeEnum.Converted)
			{
				logger.LogInformation("Document was converted");
				Console.WriteLine("Document was converted");
				await this.DownloadConvertedContract(string.Empty,string.Empty, srEvent.Document.Uuid);				
			}

			if (srEvent.EventType == Event.EventTypeEnum.Signed)
			{
				logger.LogInformation("Document was signed");
				Console.WriteLine("Document was signed");
				await this.DownloadSignedContract(string.Empty,string.Empty, srEvent.Document.Uuid);
				await this.DownloadSigningLog(string.Empty,string.Empty, srEvent.Document.Uuid);
			}
		}

		public async Task DownloadConvertedContract(string resellerId, string contractNumber, string documentUuid)
		{
			Console.WriteLine($"start to get converted contract {documentUuid}");
			var srDocument = ReadDocument(documentUuid);
			if (srDocument == null)
			{
				Console.WriteLine($"converted contract not found {documentUuid}");
				return;
			}

			Console.WriteLine($"start to download converted contract {srDocument.FileAsPdf}");			
			var webClient = new System.Net.WebClient();
			var downloadedFile = await webClient.DownloadDataTaskAsync(srDocument.FileAsPdf);

			Console.WriteLine("start to upload converted contract to minio");
			// this.Order.ResellerId + "/" + this.OrderId + "/" + this.Order.OrderNumber;
			string filename = string.IsNullOrEmpty(contractNumber) ? $"contract_converted_{Guid.NewGuid().ToString()}.pdf" : $"{contractNumber}/contract_converted.pdf";
			if (!string.IsNullOrEmpty(resellerId))
			{
				filename = resellerId + "/" + filename;
			}
			await fileService.UploadDocument("contracts", filename, 
				new MemoryStream(downloadedFile), downloadedFile.LongLength, "application/pdf");			
		}

		public async Task DownloadSignedContract(string resellerId, string contractNumber, string documentUuid)
		{
			Console.WriteLine($"start to get signed contract {documentUuid}");
			var srDocument = ReadDocument(documentUuid);
			if (srDocument == null)
			{
				Console.WriteLine($"signed contract not found {documentUuid}");
				return;
			}

			Console.WriteLine($"start to download signed contract {srDocument.Pdf}");
			var webClient = new System.Net.WebClient();
			var downloadedFile = await webClient.DownloadDataTaskAsync(srDocument.Pdf);

			Console.WriteLine("start to upload signed contract to minio");
			string filename = string.IsNullOrEmpty(contractNumber) ? $"contract_signed_{Guid.NewGuid().ToString()}.pdf" : $"{contractNumber}/contract_signed.pdf";
			if (!string.IsNullOrEmpty(resellerId))
			{
				filename = resellerId + "/" + filename;
			}
			await fileService.UploadDocument("contracts", filename,
				new MemoryStream(downloadedFile), downloadedFile.LongLength, "application/pdf");
		}

		public async Task DownloadSigningLog(string resellerId, string contractNumber, string documentUuid)
		{
			Console.WriteLine($"start to get signing log {documentUuid}");
			var srDocument = ReadDocument(documentUuid);
			if (srDocument == null)
			{
				Console.WriteLine($"signing log not found {documentUuid}");
				return;
			}

			Console.WriteLine($"start to download signing log {srDocument.SigningLog.Pdf}");
			var webClient = new System.Net.WebClient();
			var downloadedFile = await webClient.DownloadDataTaskAsync(srDocument.SigningLog.Pdf);

			Console.WriteLine("start to upload signing log to minio");
			string filename = string.IsNullOrEmpty(contractNumber) ? $"signinglog_{Guid.NewGuid().ToString()}.pdf" : $"{contractNumber}/contract_signinglog.pdf";
			if (!string.IsNullOrEmpty(resellerId))
			{
				filename = resellerId + "/" + filename;
			}
			await fileService.UploadDocument("contracts", filename,
				new MemoryStream(downloadedFile), downloadedFile.LongLength, "application/pdf");
		}


		public SignRequest.Model.Document ReadDocument(string documentUuid)
		{
			SetClientConfiguration();
			var documentsApi = new DocumentsApi();
			return documentsApi.DocumentsRead(documentUuid);
		}

		private void CreateDocumentFromConvertedEvent(string orderId,
			string documentUuid)
		{			
			Domain.Document document = new Domain.Document();			
			document.OrderId = orderId;
			document.Uuid = documentUuid;
			_context.Add(document);
			_context.SaveChanges();
		}

		private void UpdateMainDocumentFromEvent(SignRequest.Model.Event srEvent, Domain.Document document)
		{
			bool changed = false;

			bool latest = true;
			foreach (var de in document.DocumentEvents)
			{
				if (de.EventDateTime > srEvent.Timestamp)
				{
					latest = false;
					break;
				}
			}

			if (latest)
			{
				document.Status = srEvent.Document.Status.ToString();
				changed = true;
			}

			if (document.SignRequestRaw == null || document.SignRequestRaw.Length == 0)
			{
				document.SignRequestRaw = srEvent.Document.ToJson();
				changed = true;
			}

			if (document.Name == null || document.Name.Length == 0)
			{
				document.Name = srEvent.Document.Name;
				changed = true;
			}

			if (changed)
			{
				this.documentsService.UpdateDocument(document);
			}
		}

		

		//oa
		public async Task<SignRequestQuickCreate> SignRequestQuickCreate(string orderId, bool? disableEmails = null)
		{
			MemoryStream originalContractStream = new MemoryStream();

			logger.LogInformation("Obtaining document from file storage");
			//GetOriginalContractStorageFileName = this.Reseller.Id + "/" + Id + "/" + OrderNumber + "_contract.pdf";
			await this.fileService.GetObject("contracts", "sample_Kikker_contract.pdf",
				(stream) => stream.CopyTo(originalContractStream));

			var srQuickCreateApi = new SignrequestQuickCreateApi();

			var signers = new List<SignRequest.Model.Signer>();

			SignRequest.Model.Signer mainSigner =
				new SignRequest.Model.Signer("nhuthanhphong@gmail.com", "phong", "nhu", true);
			mainSigner.Language = Signer.LanguageEnum.Nl;

			signers.Add(mainSigner);

			var signData = new SignRequest.Model.SignRequestQuickCreate(
				"phong.nhu@infodation.vn", "phongnhu", null, "http://www.infodation.nl/",
				null, null, true, true, true, disableEmails, null, null,
				"Kikker contract", "pls take a look on contract and sign it", SignRequest.Model.SignRequestQuickCreate.WhoEnum.O,
				null, signers, null, null, null, orderId, null, null,
				"http://office.infodation.vn:31380/signrequest/api/Webhook?orderId=" + orderId,
				System.Convert.ToBase64String(originalContractStream.ToArray()),
				"sample_Kikker_contract.pdf", 
				null, null, null, null, null);

			logger.LogInformation("Sending request to SR to make signing document");
			Console.WriteLine("Sending request to SR to make signing document");

			SetClientConfiguration();

			return await srQuickCreateApi.SignrequestQuickCreateCreateAsync(signData);
		}

		private void SetClientConfiguration()
		{
			SignRequest.Client.Configuration.Default.AddApiKey("Authorization", "4ae303e268bf979c30f119a834bd8cc688844da6");
			SignRequest.Client.Configuration.Default.AddApiKeyPrefix("Authorization", "Token");
		}

		
	}
}

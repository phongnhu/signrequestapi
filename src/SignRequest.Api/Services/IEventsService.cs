﻿using SignRequest.Api.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignRequest.Api.Services
{
	public interface IEventsService
	{
		DocumentEvent SaveNewDocumentEvent(DocumentEvent documentEvent);
		IEnumerable<DocumentEvent> GetDocumentEvents(string documentId);
	}
}

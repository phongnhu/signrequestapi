﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SignRequest.Api.Domain;

namespace SignRequest.Api.Services
{
	public class DocumentsService : IDocumentsService
	{
		private readonly ILogger logger;
		private readonly SignRequestDbContext dbContext;

		public DocumentsService(ILogger<DocumentsService> logger, SignRequestDbContext dbContext)
		{
			this.logger = logger;
			this.dbContext = dbContext;
		}

		public Document GetDocument(string uuid)
		{
			return dbContext.Documents.Where(d => d.Uuid == uuid).Include(d => d.DocumentEvents).FirstOrDefault();
		}

		public IEnumerable<Document> GetDocumentByOrder(string orderId)
		{
			return dbContext.Documents.Where(d => d.OrderId == orderId).Include(d => d.DocumentEvents).ToList();
		}

		public Document UpdateDocument(Document document)
		{
			dbContext.Update(document);
			dbContext.SaveChanges();
			return document;
		}
	}
}

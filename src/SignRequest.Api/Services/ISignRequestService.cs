﻿using SignRequest.Api.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignRequest.Api.Services
{
	public interface ISignRequestService
	{
		Task ProcessEvent(SignRequest.Model.Event srEvent);
		Task ProcessEvent(string orderId, SignRequest.Model.Event srEvent);
		Task ProcessEvent(string resellerId, string contractNumber, SignRequest.Model.Event srEvent);
		Task<SignRequest.Model.SignRequestQuickCreate> SignRequestQuickCreate(string orderId, bool? disableEmails = null);
	}
}

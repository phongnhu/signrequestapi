﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SignRequest.Api.Services
{
	public interface IFileService
	{
		Task UploadDocument(string bucketName, string fileName, MemoryStream stream, long size, string contentType = null, Dictionary<string, string> metaData = null);
		Task GetObject(string bucketName, string fileName, Action<Stream> stream);
	}
}

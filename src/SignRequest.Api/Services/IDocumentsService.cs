﻿using SignRequest.Api.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignRequest.Api.Services
{
	public interface IDocumentsService
	{
		Document GetDocument(string uuid);
		IEnumerable<Document> GetDocumentByOrder(string orderId);
		Document UpdateDocument(Document document);
	}
}

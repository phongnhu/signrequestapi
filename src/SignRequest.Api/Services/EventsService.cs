﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SignRequest.Api.Domain;

namespace SignRequest.Api.Services
{
	public class EventsService : IEventsService
	{
		private readonly ILogger logger;
		private readonly SignRequestDbContext dbContext;

		public EventsService(ILogger<EventsService> logger, SignRequestDbContext dbContext)
		{
			this.logger = logger;
			this.dbContext = dbContext;
		}

		public IEnumerable<DocumentEvent> GetDocumentEvents(string documentId)
		{
			return dbContext.Events.Where(e => e.DocumentId == documentId).ToList();
		}

		public DocumentEvent SaveNewDocumentEvent(DocumentEvent documentEvent)
		{
			dbContext.Add(documentEvent);
			dbContext.SaveChanges();
			return documentEvent;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SignRequest.Api.Domain;
using SignRequest.Api.Services;

namespace SignRequest.Api.Controllers
{
	[ApiController]
	public class DocumentsController : Controller
    {
		private readonly ILogger logger;
		private readonly IDocumentsService documentsService;

		public DocumentsController(ILogger<DocumentsController> logger,
			IDocumentsService documentsService)
		{
			this.logger = logger;
			this.documentsService = documentsService;
		}

		[Route("signrequest/api/document")]
		[HttpGet]
		public ActionResult Get(string uuid)
		{
			try
			{
				//234b6ff8-04e3-4403-b856-279167587d40
				var result = documentsService.GetDocument(uuid);
				return Ok(result);
			}
			catch (Exception ex)
			{
				logger.LogError("Exception when calling GetDocument: " + ex.Message);
				return this.StatusCode(500, ex.Message);
			}
		}

		[Route("signrequest/api/documents")]
		[HttpGet]
		public ActionResult GetByOrder(string orderId)
		{			
			try
			{				
				var result = documentsService.GetDocumentByOrder(orderId);
				return Ok(result);
			}
			catch (Exception ex)
			{
				logger.LogError("Exception when calling GetDocumentByOrder: " + ex.Message);
				return this.StatusCode(500, ex.Message);
			}
		}		
	}
}
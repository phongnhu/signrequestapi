﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SignRequest.Api.Services;

namespace SignRequest.Api.Controllers
{	
	[ApiController]
	public class SignRequestController : ControllerBase
	{		
		private readonly ISignRequestService signRequestService;
		private readonly ILogger logger;

		public SignRequestController(ILogger<SignRequestController> logger,
			ISignRequestService signRequestService)
		{
			this.logger = logger;
			this.signRequestService = signRequestService;
		}

		[Route("signrequest/api/QuickCreate")]
		[HttpPost]
		public async Task<IActionResult> QuickCreate([FromQuery] string orderId)
		{
			Console.WriteLine($"start QuickCreate order {orderId}");
			await signRequestService.SignRequestQuickCreate(orderId);
			return Ok();
		}


		[Route("signrequest/api/WebhookPublic")]
		[HttpPost]
		public async Task<IActionResult> WebhookPublic([FromBody] SignRequest.Model.Event data)
		{
			Console.WriteLine($"Incoming event {data.EventType.Value.ToString()}");
			await signRequestService.ProcessEvent(data);
			return Ok();
		}

		//[Route("signrequest/api/Webhook")]
		//[HttpPost]
		//public async Task<IActionResult> WebhookPublic([FromQuery] string orderId,
		//	[FromBody] SignRequest.Model.Event data)
		//{
		//	Console.WriteLine($"Start to process order {orderId}, event {data.EventType.Value.ToString()}");
		//	await signRequestService.ProcessEvent(orderId, data);
		//	return Ok();
		//}

		[Route("signrequest/webhook")]
		[HttpPost]
		public async Task<IActionResult> WebhookPublic([FromQuery] string resellerId, [FromQuery] string contractId,
			[FromBody] SignRequest.Model.Event data)
		{
			Console.WriteLine($"Event {data.EventType.Value.ToString()}");
			Console.WriteLine($"Start to process contract {contractId}, reseller {resellerId}");
			await signRequestService.ProcessEvent(resellerId, contractId, data);
			return Ok();
		}
	}
}
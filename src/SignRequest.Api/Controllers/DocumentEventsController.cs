﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SignRequest.Api.Services;

namespace SignRequest.Api.Controllers
{
    public class DocumentEventsController : ControllerBase
    {
		private readonly ILogger logger;
		private readonly IEventsService eventsService;

		public DocumentEventsController(ILogger<DocumentEventsController> logger, 
			IEventsService documentsService)
		{
			this.logger = logger;
			this.eventsService = documentsService;
		}

		[Route("signrequest/api/events")]
		[HttpGet]
		public ActionResult GetDocumentEvents(string documentId)
		{
			try
			{
				var result = eventsService.GetDocumentEvents(documentId);
				return Ok(result);
			}
			catch (Exception ex)
			{
				logger.LogError("Exception when calling GetDocumentEvents: " + ex.Message);
				return this.StatusCode(500, ex.Message);
			}
		}
	}
}
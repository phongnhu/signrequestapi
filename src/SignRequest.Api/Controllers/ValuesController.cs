﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SignRequest.Api.Services;
using SignRequest.Client;
using SignRequest.Model;

namespace SignRequest.Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ValuesController : ControllerBase
	{
		const string YOUR_API_KEY = "4ae303e268bf979c30f119a834bd8cc688844da6";
		private readonly ILogger logger;
		private readonly ISignRequestService signRequestService;

		public ValuesController(ILogger<ValuesController> logger, ISignRequestService signRequestService)
		{
			this.logger = logger;
			this.signRequestService = signRequestService;
		}

		[Route("WebhookPublic")]
		[HttpPost]
		public async Task<IActionResult> WebhookPublic([FromBody] SignRequest.Model.Event data)
		{
			await signRequestService.ProcessEvent(data);
			return Ok();
		}

		// GET api/values
		[HttpGet]
		public ActionResult<IEnumerable<string>> Get()
		{
			return new string[] { "value1", "value2" };
		}

		// GET api/values/5
		[HttpGet("{id}")]
		public ActionResult<Document> Get(string id)
		{
			try
			{
				//fff87304-67ac-47e9-8891-79d21f059217
				var result = GetDocument(id);
				return Ok(result);
			}
			catch (Exception ex)
			{
				logger.LogError("Exception when calling DocumentsApi.DocumentsRead: " + ex.Message);
				return this.StatusCode(500, ex.Message);
			}
		}

		private Document GetDocument(string uuid)
		{
			// Configure API key authorization: Token
			Configuration.Default.AddApiKey("Authorization", "4ae303e268bf979c30f119a834bd8cc688844da6");
			Configuration.Default.AddApiKeyPrefix("Authorization", "Token");

			var apiInstance = new DocumentsApi();

			// Retrieve a Document - entire Document Model object is defined in NuGet package
			Document result = apiInstance.DocumentsRead(uuid);
			Console.WriteLine("Document PDF url: " + result.FileAsPdf);
			logger.LogInformation("Document PDF url: " + result.FileAsPdf);
			return result;
		}

		// POST api/values
		[HttpPost]
		public void Post([FromBody] string value)
		{
		}

		// PUT api/values/5
		[HttpPut("{id}")]
		public void Put(int id, [FromBody] string value)
		{
		}

		// DELETE api/values/5
		[HttpDelete("{id}")]
		public void Delete(int id)
		{
		}
	}
}

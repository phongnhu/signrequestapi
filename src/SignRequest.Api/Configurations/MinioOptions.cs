﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignRequest.Api.Configurations
{
	public class MinioOptions
	{
		public string Host { get; set; }
		public string Key { get; set; }
		public string Secret { get; set; }
	}
}

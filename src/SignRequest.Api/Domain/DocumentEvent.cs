﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SignRequest.Api.Domain
{
	public class DocumentEvent
	{
		[Key]
		public string DocumentEventId { get; set; }
		[ForeignKey("Document")]
		public string DocumentId { get; set; }
		public Document Document { get; set; }
		public string Status { get; set; }
		public DateTime? EventDateTime { get; set; }
		public string EventRawData { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SignRequest.Api.Domain
{
	public class Document
	{
		[Key]
		public string Uuid { get; set; }
		public string OrderId { get; set; }
		public string Status { get; set; }
		public string Name { get; set; }
		public string OriginalContract { get; set; }
		public string ConvertedContract { get; set; }
		public string SignedContract { get; set; }
		public string SigningLog { get; set; }
		public string SignRequestRaw { get; set; }
		public virtual IEnumerable<DocumentEvent> DocumentEvents { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SignRequest.Api.Domain
{

	public class Order
	{
		//[Key]
		public string Id { get; set; }

		//[DisplayName("Order Number")]
		//[Required]
		public string OrderNumber { get; set; }

		//[DisplayName("Order date/time")]
		public DateTime Timestamp { get; set; }

		//[ForeignKey("Reseller")]
		//[DisplayName("Reseller")]
		public string ResellerId { get; set; }

		//public Reseller Reseller { get; set; }

		//[DisplayName("Customer First Name")]
		public string CustomerName { get; set; }

		//[DisplayName("Customer Last Name")]
		public string CustomerLastName { get; set; }

		//[DisplayName("Customer Email")]
		//[Required]
		public string CustomerEmail { get; set; }

		//[DisplayName("Email Title")]
		public string EmailTitle { get; set; }

		//[DisplayName("Email Message")]
		public string EmailMessage { get; set; }

		public Document Contract { get; set; }

		//[DisplayName("Event webhook")]
		public string Webhook { get; set; } = "http://office.infodation.vn:31380/signrequest/api/Webhook?orderId=";

		//[NotMapped]
		//[DisplayName("Upload contract (generated in Kikker system)")]
		//public IFormFile OriginalContract { get; set; }

		public string GetOriginalContractStorageFileName()
		{
			return Id + "/" + OrderNumber + "_contract.pdf";
		}

		public string GetOriginalContractSignRequestFileName()
		{
			return OrderNumber + ".pdf";
		}
	}
	
}

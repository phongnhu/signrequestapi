﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignRequest.Api.Domain
{
	public class SignRequestDbContext: DbContext
	{
		public SignRequestDbContext(DbContextOptions<SignRequestDbContext> options) : base(options)
		{

		}
		public DbSet<Domain.Document> Documents { get; set; }
		public DbSet<Domain.DocumentEvent> Events { get; set; }
	}
}
